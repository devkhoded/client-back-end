module.exports = {


  friendlyName: 'Create',


  description: 'Create product.',


  inputs: {
    name:{
      type:'string',
    },
    price:{
      type:'number',
    },
    url:{
      type:'string',
    }
  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs, exits) {
    var productRecord= await Product.create({
      name: inputs.name,
      url: inputs.url,
      price: inputs.price
    })
 .fetch();
    if (!productRecord) {
      return exits.invalid({
        message: 'Invalid, Problem creating Product'
      });
    }
    return exits.success({
      message:'Product has been created successfully',
      data: productRecord
    });
  }
};
