module.exports = {


  friendlyName: 'Find',


  description: 'Find product.',


  inputs: {
    productId: {
      type: 'number',
      required: true,
    },
  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs, exits) {
    var findProduct = await Product.findOne({
      where: {id: inputs.productId},
      select: ['name', 'price']
    });
    if (!findProduct) {
      return exits.invalid({
        message: 'Product does not exist'
      });
    }
    return exits.success({
      message: 'Product Found.',
      data: findProduct
    });

  }


};
