const twilioSid = process.env.SID;
const twilioauthToken = process.env.AUTH;
const twiliomobilenumber= '+12057082553';
const twilioTo = '+234058515370';
const twilioClient = require('twilio')(twilioSid, twilioauthToken);

module.exports = {


  friendlyName: 'Send sms',


  description: 'Send SMS via twillio',


  inputs: {
  toMobileNumber: {
       type: 'string',
       required: true
     },
  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid request'
    },
  },


  fn: async function (inputs, exits) {
     console.log("helper: send-sms: ", inputs);
    var allProducts = await Cart.find({
      select: ['name', 'price']
    });
    if (!allProducts) {
      return exits.invalid({
        message: 'Empty Cart'
      });
    }
    const products = [];
    allProducts.forEach((d) => {
      const obj = {};
      obj['Product name​'] = d.name;
      obj['Product Price​'] = d.price;
      products.push(obj);
    });
    twilioClient.messages.create({
      body: 'You made the following Order'+JSON.stringify(products).replace(/[{}\[\]"]+/g,''),
      to: inputs.toMobileNumber, // Text this number
      from: twiliomobilenumber// From a valid Twilio number
    }, (err, message) => {
      if (message) {
            return exits.success({
              status: true,
              message: "sms send success",
              data: message
            });
          } else if (err) {
            var err = new Error({
              type: 'Twilio',
              status: false,
              message: err.message
            });
            return exits.error(err);
          } else {
            var err = new Error({
              type: 'Twilio',
              status: false,
              message: "Twillio Error"
            });
            return exits.error(err);
          }
        });
  }

};
