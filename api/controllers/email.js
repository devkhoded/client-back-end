const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.APIKEY);
module.exports = {


  friendlyName: 'Send email',


  description: '',


  inputs: {
    toEmail: {
      description: 'Mobile Number to which we need to send test sms.',
      type: 'string',
      required: true
    },
  },


  exits: {
    success: {
      description: 'All done.',
    },
  },


  fn: async function (inputs, exits) {
    var allProducts = await Cart.find({
      select: ['name', 'price']
    });
    if (!allProducts) {
      return exits.invalid({
        message: 'Empty Cart'
      });
    }
    const products = [];
    allProducts.forEach((d) => {
      const obj = {};
      obj['Product name​'] = d.name;
      obj['Product Price​'] = d.price;
      products.push(obj);
    });

    const msg = {
      to: inputs.toEmail,
      from: 'adewaleonamade@gmail.com',
      subject: 'Hey here is your order',
      text: 'You made the following Order'+JSON.stringify(products).replace(/[{}\[\]"]+/g,''),
    //  html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    };
    sgMail.send(msg);
    return exits.success({
      status: true,
      message: "email delivered success",
      data: msg
    });
  }


};
