module.exports = {


  friendlyName: 'Update',


  description: 'Update product.',


  inputs: {
    productId: {
      type: 'number',
      required: true,
    },
    id:{
      type:'number'
    },
    url:{
      type: 'string'
    },
    price:{
      type: 'number'
    },
    name:{
      type:'string'
    },
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    var updatedProduct = await Product.update({
      id: inputs.productId
    }).set({
      name: inputs.name,
      url: inputs.url,
      price: inputs.price,
    })
    .fetch();
    return exits.success({
      message: 'Product has been updated successfully.',
      data: updatedProduct
    });

  }


};
