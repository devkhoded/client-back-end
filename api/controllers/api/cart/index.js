module.exports = {


  friendlyName: 'Index',


  description: 'Index Cart.',


  inputs: {

  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs) {
    var allCarts = await Cart.find({});
    if (!allCarts) {
      return exits.invalid({
        message: 'No available Cart'
      });
    }
    return allCarts;

  }


};
