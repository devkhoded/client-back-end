module.exports = {


  friendlyName: 'Create',


  description: 'Create user.',


  inputs: {
    fullName:{
      type: 'string',
      unique:true,
      required:true
    },
    email: {
      required: true,
      unique: true,
      type: 'string',
      isEmail: true,
    },
    password: {
      required: true,
      type: 'string',
      maxLength: 15,
      minLength: 6,
    },
    phoneNumber: {
      type: 'number',
      required: true,
    }
  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'firstname, lastname, phone , email and password is required.'
    },
    redirect: {
      responseType: 'redirect'
    }
  },

  fn: async function (inputs, exits) {
    var userRecord = await User.create({
      firstName: inputs.firstName,
      lastName: inputs.lastName,
      email: inputs.email,
      password: inputs.password,
      phoneNumber:inputs.phoneNumber
    })
       .intercept('E_UNIQUE', (err) => {
         return exits.invalid({
           message: 'invalid',
           err: err
         });
       })
           .fetch();

    if (!userRecord) {
      return exits.invalid({
        message: 'invalid, problem creating user'
      });
    }
    return exits.success({
      message: 'User has been created successfully.',
      data: userRecord
    });
  }
};
