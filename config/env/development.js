module.exports = {
  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  models: {},

  //   /****** twilio ******/
  twilioSid: 'AC78c7be37822015c4bac5c225d866a41f',
  twilioauthToken: "5c814aaa5edf55dfebb1d6d2bcaf7123",
  twiliomobilenumber: '+12057082553',
  twilioTo:'+234058515370',

  url: {
    appURL: "http://localhost:1337",
    applicationURL: "http://localhost:1337",
  },

  mode: "development",
};
