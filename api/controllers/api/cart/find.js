module.exports = {


  friendlyName: 'Find',


  description: 'Find cart.',


  inputs: {
    cartId: {
      type: 'number',
      required: true,
    },
  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs, exits) {
    var findCart = await Cart.findOne({
      where: {id: inputs.cartId},
      select: ['name', 'price']
    });
    if (!findCart) {
      return exits.invalid({
        message: 'Cart does not exist'
      });
    }
    return exits.success({
      message: 'Cart Found.',
      data: findCart
    });

  }


};
