module.exports = {


  friendlyName: 'Create',


  description: 'Add  Product to Cart',


  inputs: {
    name:{
      type:'string',
      required:true
    },
    price:{
      type:'number',
      required: true
    },
  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs, exits) {
    var CartRecord= await Cart.create({
      name: inputs.name,
      price: inputs.price
    })
 .fetch();
    if (!CartRecord) {
      return exits.invalid({
        message: 'Invalid, Problem adding to Cart'
      });
    }
    return exits.success({
      message:'Product  has been added Cart successfully',
      data: CartRecord
    });
  }
};
