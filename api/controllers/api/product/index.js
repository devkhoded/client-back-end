module.exports = {


  friendlyName: 'Index',


  description: 'Index product.',


  inputs: {

  },


  exits: {
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs) {
    var allProducts = await Product.find({});
    if (!allProducts) {
      return exits.invalid({
        message: 'No available Product'
      });
    }
    return allProducts;

  }


};
