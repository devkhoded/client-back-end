module.exports = {


  friendlyName: 'Delete',


  description: 'Delete product.',


  inputs: {
    productId: {
      type: 'string',
    },
  },


  exits: {
    success: {
      description: 'product Deleted.',
    },
    redirect: {
      responseType: 'redirect'
    },
    invalid: {
      statusCode: 409,
      description: 'Invalid requiest'
    },
  },


  fn: async function (inputs, exits) {

    var productId = inputs.productId;
    console.log('delete request get id :- ', productId);

    var productRecord = await Product.destroy({
      id: productId
    }).fetch();

    if (!productRecord) {
      return exits.invalid({
        message: 'invalid could not delete Product'
      });
    }


    exits.success({
      message: 'Product has been deleted successfully.',
      data: productRecord
    });

  }


};
